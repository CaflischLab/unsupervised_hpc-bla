Scripts supporting the following article:

Unsupervised methods for detection of neural states: case study of hippocampal-amygdala interactions
---
**Francesco Cocina**, **Andreas Vitalis**\*, **Amedeo Caflisch**\*

Published in *eNeuro*, 8(6), 2021. DOI:[10.1523/ENEURO.0484-20.2021](https://doi.org/10.1523/ENEURO.0484-20.2021)

Abstract: *The hippocampus and amygdala are functionally coupled brain regions that play a crucial role in processes involving memory and learning. 
Because inter-areal communication has been reported both during specific sleep stages and in awake, behaving animals, these brain regions can serve as an archetype to establish that measuring functional interactions is important for comprehending neural systems.To this end, we analyze here a public data set of local field potentials (LFPs) recorded in rats simultaneously from the hippocampus and amygdala during different behaviors. Employing a specific, time-lagged embedding technique, named topological causality, we infer directed interactions between the LFP band powers of the two regions across six frequency bands in a time-resolved manner. The combined power and interaction signals are processed with our own unsupervised tools developed originally for the analysis of molecular dynamics simulations to effectively visualize and identify putative, internal states that are visited by the animals repeatedly. Our proposed methodology minimizes impositions onto the data, such as isolating specific epochs, or averaging across externally annotated behavioral stages, and succeeds in separating internal states by external labels such as sleep or stimulus events. We show that this works better for 2 of the 3 rats we analyzed, and highlight the need to acknowledge individuality in analyses of this type. Importantly, we demonstrate that the quantification of functional interactions is a significant factor in discriminating these external labels, and we suggest our methodology as a general tool for large, multi-site recordings.* 


---
---


&nbsp;  
Below you can find the description of the files in the `scripts/` directory.  
R files have a preamble with library loading, definition of general variables, sourcing of utils files and definition of directories needed for reading/writing.  
The matlab files take generally as input a .txt file containing the arguments (convenient for being launched in series).  

`Matlab>=2018b` and `R>=3.6.3` are highly recommended. Scripts have been tested only on linux systems.  
[Campari version 3](http://campari.sourceforge.net/) has been used for the progress index analysis.  


#### Data extraction from repository

`archive_lfp_power.m` &rarr; Computes the spectrograms and the band powers.  
`archive_lfp_wavelet.m` &rarr; Computes the wavelet transform of the LFPs and extracts phase and amplitude.  
`archive_spikes.m` &rarr; Extracts the spikes of the relevant anatomical structures and save them in a friendly format.  
`archive_velocities.m` &rarr; Extracts the positions of the two LEDs and computes the velocities.  

#### Topological causality analysis

`topoc_raw.R` &rarr; Computes the topological causality (TC) values.  
`topoc_raw_utils.R` &rarr; Utils.  

`features_extraction.R` &rarr; Extract useful labels/features of the dataset and combines the TCs in a unique data.frame for each session.  
`features_extraction_labels_utils.R` &rarr; Utils; extraction of the labels.  
`features_extraction_spikes_utils.R` &rarr; Utils; extraction and computation related to spikes.  

`topoc_summary.R` &rarr; TC mean values shown in Figures 2, 3a, 2-1, 3-1, 3-2.  
`topoc_summary_other_utils.R` &rarr; Utils.  
`topoc_summary_general_utils.R` &rarr; Utils used in various scripts.  

`topoc_alignment.R` &rarr; Alignment analysis of Figures 3-3, 3-4.  
`topoc_alignment_utils.R` &rarr; Utils.  

`phase_difference.R` &rarr; LFPs' phase difference for volume conduction investigation (Figure 2-2).  

#### Cross correlation and Granger causality

`ccf_creation.R` &rarr; Computes the cross correlation functions (CCF) and their chance level by block permutation.  
`ccf_processing.R` &rarr; Extracts and summarize the CCF results.    
`granger_creation.m` &rarr; Computes the Granger causality (GC) values on sliding windows.  
`granger_processing.R` &rarr; Extracts and summarizes the GC values.  
`topoc_ccf_granger_comparison.R` &rarr; Compare the TC, GC and CCF values as shown in Figure 2bc.  

#### Progress-index and Sapphire-based clustering

`pidx_pipeline.R` &rarr; Creation and preprocessing of the combined data set of LFPs and TCs. Preparation and running of the progress index (PI) algorithm.  
`pidx_pipeline_utils.R` &rarr; Utils; used also in other scripts.  

`clustering_sapphire.R` &rarr; Runs the SbC method.  
`clustering_sapphire_utils.R` &rarr; Utils.  
`clustering_sapphire_functions.R` &rarr; Utils; SbC and rearrangement functions.  
`clustering_other.R` &rarr; Runs the other clustering methods.  

`affinity_scores.R` &rarr; Affinity scores calculation.  
`affinity_scores_topoc.R` &rarr; Affinity scores for TCs. These will be used then in the biplot (Figures 5e, 6-2g).  

`matching_states_rats.R` &rarr; Matching of states across rats (colored bars below the SAPPHIRE plots) using the affinity scores.  

`sapphire_plot.R` &rarr; SAPPHIRE plots creation (Figures 4, 4-2:4-7).  
`sapphire_plot_utils.R` &rarr; Utils.  

#### Unfolding

`unfolding.R` &rarr; Unfolding calculation on the single rats.  
`unfolding_allrats.R` &rarr; Unfolding calculation on the joint dataset of the three rats and calculation of the data for the biplot.  
`unfolding_utils.R` &rarr; Utils.  
`unfolding_plot.R` &rarr; Assemblying of all the unfolding projections (Figures 5bcde and 6-2a-d).  
`unfolding_stress_analysis.R` &rarr; Analysis of the stress values: comparison SbC with other methods, stress per label, shuffling of TCs (Figure 6, 6-1, 6-2efg).  

#### Others

`utils.R` &rarr; General utils.  

`data/RatXX-aversive.txt` &rarr; Aversive direction (with air puff) for each session. L2R: from left to right (from the video).  
