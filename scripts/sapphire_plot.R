library(data.table)
library(RColorBrewer)
library(tidyverse)
library(scales)
library(cowplot)
options(scipen=999) ## Turn scientific notaion

behav.list <- c("prerun", "presleep", "run", "postsleep", "postrun")
bands <- c("delta", "theta", "spindles", "lowGamma", "highGamma", "ripples")
brain.str <- c("awake", "sws", "rem")

source("utils.R")
source("topoc_summary_general_utils.R")
source("sapphire_plot_utils.R")
dir.matlab <- here::here("results/matlab/")
dir.analysis <- here::here("results/general/")
dir.rdata.nodrd <- here::here("results_campari/RData_nodrd/")
dir.pidx <- here::here("results_campari/pidx/")
dir.clustering <- here::here("results/clustering/")
dir.plots <- here::here("plots/")
ncores <- 6


## #############################################################################################
## Main Settings



cat(lt(file.list), "\n")
file.list <- list.files(dir.pidx,
                        full.names=T) %>%
    str_subset("-allsess-bla_right-ftspow20Hz-behall-setbbtc-")
file.npc18 <- str_subset(file.list, "-wghnull-drdPCA-npc18") %>%
    str_subset("^((?!msh).)*$") %>%
    str_subset("^((?!del).)*$")
file.npc5 <- str_subset(file.list, "-wghLAW-drdPCA-npc5") %>%
    str_subset("^((?!msh).)*$") %>%
    str_subset("^((?!del).)*$")
file.list <- c(file.npc5, file.npc18)

## #########
list_fig <- c("Figure_4-Rat11-npc18",
              "Figure_4-2_Rat08-npc18",
              "Figure_4-3_Rat10-npc18",
              "Figure_4-4_Rat11-npc18-barrier",
              "Figure_4-5_Rat08-npc5",
              "Figure_4-6_Rat10-npc5",
              "Figure_4-7_Rat11-npc5")


walk(file.list, function(file.pix) {
    cat("START\n ")
    
    ## #################
    ## Extracting parameters and options
    
    file.root <- str_remove(basename(file.pix), "_[0-9]{12}.pidx")
    rat <- str_extract(file.root, "Rat[0-9]{1,2}")
    session <- str_extract(file.root, "Rat[0-9]{1,2}-([0-9]{8}|allsess)")
    anatom <- str_extract(file.root, "bla_(left|right)")
    opt <- decompose.name(str_remove(file.root, str_glue("{session}-{anatom}-")))

    behav.pi <- switch(as.character(opt$beh),
                       all = behav.list,
                       allrun = str_subset(behav.list, "run"),
                       allsleep = str_subset(behav.list, "sleep"))

    if(str_detect(session, "allsess")) {
        if(rat == "Rat10") {
            session <- c("Rat10-20140622", "Rat10-20140624", "Rat10-20140626", "Rat10-20140627",
                         "Rat10-20140628", "Rat10-20140702", "Rat10-20140703")
        } else if(rat == "Rat08") {
            session <- c("Rat08-20130709", "Rat08-20130710", "Rat08-20130711", "Rat08-20130712",
                         "Rat08-20130715",
                         ## "Rat08-20130716",
                         "Rat08-20130717")
        }  else if(rat == "Rat11") {
            session <- c("Rat11-20150325", "Rat11-20150326", "Rat11-20150327",
                         "Rat11-20150328", "Rat11-20150330", "Rat11-20150331")
        }
    }
    
    cat(file.root, "\n")
    
    ## #################
    ## TCS reading to tail...

    ## Extracting only the information of the lengths and time
    tcs <- map.c(session, function(sess) {
        cat("Reading tcs", sess," \n")
        tcs <- readRDS(str_glue("{dir.analysis}{sess}-{anatom}-power-topoc_dataframe.RData"))[behav.pi]
        lengths <- map(tcs, ~ min(sapply(.x, nrow)))
        time.tcs <- map2(tcs, lengths, ~ tail(.x[[1]]$time, .y))
        return(list(tcs = tcs, lengths = lengths, time.tcs = time.tcs))
    })
    lengths <- map(tcs, "lengths")
    time.tcs <- map(tcs, "time.tcs")
    rm(tcs)

    ## Retrieving the actual 72 TCs to be eventually used for displaying in the SAPPHIRE plot
    ## i.e. after preprocessing (without weight) if present
    tcs.list <- list.files(dir.rdata.nodrd) %>%
        str_subset(str_extract(file.root, str_glue("{rat}-([0-9]{8}|allsess)-{anatom}"))) %>%
        str_subset(name.collapse(select(opt, matches("(beh|set|[a-z]tc)"))))
    if(lt(tcs.list) != 0) {
        tcs.file <- paste0(dir.rdata.nodrd, basename(tcs.list[1]))
        tcs <- readRDS(tcs.file) %>% select(matches("(alpha|tc[1-2]{2})"))
    } else stop("There are not templates....")            

    ## #################
    ## LABELS reading

    state_ofhpc <- readRDS(str_glue("{dir.analysis}{rat}-bla_right-win2s-brain_states_hpc_REBUTTAL.RData"))
    lab <- map.c(session, function(sess) {
        cat("Reading labels", sess," \n")
        lab <- readRDS(str_glue("{dir.analysis}{sess}-{anatom}-win2s-labels_dataframe.RData"))[behav.pi]
        if(str_detect(opt$set, "tc") ) {
            lab <- map2(lab, lengths[[sess]], ~ tail(.x, .y))
        }
        
        ## Substitung new Hpc sleep state definition
        lab <- map_at(lab, c("presleep", "postsleep"), function(df) {
            left_join(df, select(state_ofhpc[[sess]], -behav), by = "time") %>%
                relocate(state_hpc, .after = "state") %>%
                select(-state) %>%
                dplyr::rename(state = "state_hpc")
        })
        
        lab <- modify_at(lab, c("prerun", "run", "postrun"),
                         ~ mutate(.x, state = factor(rep("awake", n()), levels = brain.str)))
        lab <- bind_rows(lab, .id = "behav")
        if( str_detect(opt$set, "tc") && any(abs(lab$time - unlist(time.tcs[[sess]])) > 0.01)) stop("Something wrong.. ")
        if(lt(session) != 1) lab <- select(lab, -matches("Ens[0-9]"))
        return(lab)
    })
    lab <- ifelse.c(lt(session) == 1, lab[[1]], bind_rows(lab, .id = "session")) 

    ## if(nrow(lab) != nrow(data.pi)) stop("Major Error in labels")

    lab <- as_tibble(lab) %>%
        mutate(behav = factor(behav, levels = behav.pi)) %>%
        mutate(state = factor(state, levels = brain.str)) %>%
        select(-matches("(hpc|bla|pure|cross|crossnaive)$"), -matches("tfidf"))

    ## ########
    ## Reading the hippocampal-bla_right theta

    theta_df <- expand_grid(session = session,
                            behav = behav.list,
                            anat = c("hippo", anatom)) %>%
        mutate(lfp = pmap(list(session, behav, anat), function(ss, bh, an) {
            nm <- paste0(dir.matlab, ss, "-", an,"-", bh, "-lfp_win2s_85q.mat")
            map_dfc(R.matlab::readMat(nm)[[1]][,,1][c("theta", "time")], as.vector)
        })) %>%
        unnest(lfp) %>%
        pivot_wider(names_from = "anat", values_from = "theta") %>%
        rename(Hpc = "hippo", "BLA" = "bla_right") %>%
        group_by(session) %>%
        mutate(across(c(Hpc, BLA), ~ as.vector(scale(.)))) %>%
        ungroup()

    lab <- left_join(lab, theta_df, by = c("session", "behav", "time"))
    rm(theta_df)
    
    ## ########################
    ## ANNOTATION Additional
    add.ann <- splice(
        SWRs = lab$rpp,
        `Theta power` = select(lab, Hpc, BLA),
        Speed = lab$vel,
        `Hpc rate` = select(lab, matches("^hippo\\.rate\\.pyr$"))[[1]],
        `BLA rate` = select(lab, matches("^bla_all\\.rate\\.pyr$"))[[1]])
    
    tcs.summ <- map_dfc(c("tc12", "tc21"), function(nm) {
        select(tcs, starts_with(nm)) %>%
            mutate(tmp = rowMeans(across(everything())), .keep = "unused") %>%
            mutate(across(tmp, scales::rescale, to = c(0,1))) %>%
            rename_with( ~ paste0(nm, ".mean"), "tmp")
    })

    if(opt$set == "bb") {
        tcs.summ <- map_depth(time.tcs, 2, ~ tibble(time = .x)) %>%
            bind_rows_rec(idd = c("behav", "session")) %>%
            bind_cols(tcs.summ) %>%
            right_join(select(lab, behav, session, time),
                       by = c("session", "behav", "time")) %>%
            select(tc12.mean, tc21.mean) %>%
            mutate(across(everything(), replace_na, 0)) 
    }            
    
    cat("Setting additional annotation \n")
    add.ann <- splice(add.ann,
                      TC = select(tcs.summ, ends_with("mean")) %>%
                          rename_at(vars(matches("tc12")), ~ "Hpc \u2192 BLA") %>%
                          rename_at(vars(matches("tc21")), ~ "BLA \u2192 Hpc"))
    
    ## ########################
    ## Setting palettes

    cat("Setting palettes \n")
    add.pal <- imap(add.ann, function(nm, ii) {
        ifelse.c(!is.data.frame(nm),
                 pals::kelly()[c(7,5,14,14)][match(ii, names(add.ann))],
                 brewer.pal(9, name="Set1")[c(2,1)]
                 )
    })
    add.pal[["Speed"]] <- pals::kelly()[c(5)]
    add.pal[["Hpc rate"]] <- brewer.pal(9, name="Set1")[c(2)]
    add.pal[["BLA rate"]] <- brewer.pal(9, name="Set1")[c(1)]
    
    if( str_detect(opt$set, "tc") ) {
        struct.pal <- ifelse.c(opt$atc == "alpha",
                               brewer.pal(11, name="RdBu"),
                               brewer.pal(9, name="YlOrBr"))
    }

    sleep.pal <- brewer.pal(8, "Greys")[c(2,4)]
    run.pal <- brewer.pal(5, "Reds")[c(3:5)]
    behav.pal <- c(run.pal, sleep.pal)[c(1,4,2,5,3)]
    brain.pal <- ifelse.c(opt$beh == "allrun",
                          NULL,
                          c("#fff7bc", brewer.pal(9, name="Set1")[c(3,4)]))

    ## ##################################
    ## Discrete annotation
    discr.ann <- select(lab, Behaviour = behav, Phase = state) %>%
        mutate_at("Phase", recode_factor, awake = "Wake", sws = "NREM", rem = "REM") %>%
        mutate_at("Behaviour", ~ factor(., levels = behav.list,
                                        labels = str_to_title(behav.list) %>%
                                            str_replace("run", "Run") %>%
                                            str_replace("sleep", "Sleep")))
    discr.pal <- list(Behaviour = behav.pal, Phase = brain.pal)
    
    
    puffs <- lab %>%
        mutate(Time = c(1:n())) %>%
        select(Time, zone) %>%
        filter(str_detect(zone, "(Airpuff|Reward)")) %>%
        mutate_at("zone", ~ str_remove(., "(Left|Right)")) %>%
        mutate_at("zone", ~ factor(., levels = c("Airpuff", "Reward"))) %>%
        droplevels()
    
    ## #################
    ## PROGIDX file reading
    arrangement <- "after"
    if(arrangement == "original") {
        
        data.pi <- data.table::fread(file.pix, data.table=FALSE, select=c(1,3,4,5)) %>%
            rename_all(~ c("PI", "Time", "Cut", "Edge")) %>%
            mutate(LocalCut = rowMeans(data.table::fread(file.pix, data.table=FALSE, select=c(10,12))))

    } else {
        file.bas <- list.files(dir.clustering, paste0(file.root, "_sbr.RData"), full.names = T)
        data.pi <- readRDS(file.bas)$after$progind %>%
                                   select(PI, Time, Cut, LocalCut)
        breaks <- readRDS(file.bas)$after$tab.st$start[-1]
    }

    cat("Reading PI \n")

    ## #####################################################
    ## ## Alignign to plot
    
    add.ann <- map(add.ann, function(df) {
        ifelse.c(is.data.frame(df), df[data.pi$Time,], df[data.pi$Time])
    })
    discr.ann <- mutate_all(discr.ann, ~ .[data.pi$Time])
    puffs <- mutate(puffs, PI= match(Time, data.pi$Time))

    ## ########################
    ## Smoothing of additional annotation for visualization purposes

    cat("Smoothing additional annotation \n")
    add.ann <- map(add.ann,
                   rollmean.gg, ## Fast rollmean that accounts also for partial windows
                   k = ifelse(opt$beh=="allrun", 11, 501)) ## 25 secs

    ## ########################
    ## Tracking the REM snapshots, too labile traces

    track_rem <- rollmean.gg(as.character(as_tibble(discr.ann)$Phase) == "REM", 801)

    ## Color on time annotation
    dynam_col <- lab$state[data.pi$Time]

    ## ########################
    ## Thresholding the theta
    
    add.ann[["Theta power"]] <- mutate_all(add.ann[["Theta power"]], ~ replace(., which(. > 5), 5))
    
    ## ################
    ## Names mapping

    if(opt$set == "bb") {
        name_fig <- str_glue("{rat}-setbb-npc18")
    } else {
        name_fig <- str_subset(list_fig, str_glue("{rat}-npc{opt$npc}$"))
    }
    cat(name_fig)

    ## ##################
    ## Annotation for the macro-states

    macro_df <- readRDS(paste0(dir.analysis, "Rats_CrossMatch_npc18.RData"))[[rat]]

    ## #####################################################
    ## ## PLOT
    
    graphics.off()
    cat("Creating plot.. \n")
    
    pts2display <- ceiling(nrow(data.pi)/(5*10^5))*2
    sapph <- ggsapphire(data.pi,
                        grain = pts2display,
                        macrostate.annot = ifelse.c(pluck(opt, "npc", .default = -1) == 18,
                                                    macro_df,
                                                    NULL),
                        macrostate.pal = setNames(brewer.pal(6, "Set1")[c(1,2,3,6)], seq(4)),
                        kinet.max = 0.99,
                        kinet.transform = T,
                        kinet.who = "LocalCut",
                        dynam.col = dynam_col,
                        dynam.col.pal = replace(brain.pal, 1, "#fee391"),
                        dynam.size.val = 0.2,
                        dynam.special = puffs,
                        dynam.special.col = c("#e31a1c", "#1f78b4"),
                        dynam.special.size = rep(1.8, 2),
                        dynam.special.legend = "Time",
                        dynam.background = NULL, 
                        dynam.background.pal = NULL, 
                        dynam.background.alpha = 0.3,
                        addit.annot = add.ann,
                        addit.pal = add.pal,
                        ## addit.ylim = ifelse.c(opt$set == "bb", NULL, ylim_add),
                        discrete.annot = discr.ann,
                        discrete.pal = discr.pal,
                        discrete.trackline = list(Phase = track_rem),
                        discrete.trackline.pal = list(Phase = discr.pal$Phase[3]),
                        discrete.geom = "segment",
                        discrete.size = 0.03,
                        rescale.xaxis = F,
                        ratios = c(ifelse.c(pluck(opt, "npc", .default = -1) == 18, 0.24, NULL),
                                   1,
                                   1.3,
                                   rep(0.2, lt(add.ann)),
                                   rep(0.25, lt(discr.ann))
                                   ),
                        custom_theme = theme_tech("google", font = "sans", custom = T) +
                            theme(panel.background = element_blank(),
                                  text = element_text(color = "black")),
                        legend = list(discrete = c("Phase", "Behaviour"),
                                      addit = c(TC = "TC", "Theta power" = "Theta power")),
                        legend.offset = c(0, 0.8),
                        legend.width = 0.11,
                        legend.rel_heights= c(1.6, 2.2, 1.3, 1.3, 1,3),
                        panel.grid.breaks = seq(1, nrow(data.pi), by = 250000),
                        plotlist = TRUE,
                        plotly = F,
                        silent = F,
                        big.size = 18,
                        medium.size = 16,
                        small.size = 10
                        )

    ## Plotting Figure 4, 4-2, 4-3, 4-4:4-6

    ggsave(str_glue("{dir.plots}/{name_fig}.png"),
           plot=sapph$pl,
           width = 17,
           height = ifelse.c(pluck(opt, "npc", .default = -1) == 18, 14, 13.2))

    
    if(!(str_detect(file.pix, "Rat11") & str_detect(file.pix, "npc18") & !str_detect(file.pix, "setbb-"))) {
        return(NULL)
    } 

    cat("Plotting also the barriers!!")
    ## ###########################################################
    ## Figure 4-4

    pts2display <- ceiling(nrow(data.pi)/(5*10^5))*2
    sapph <- ggsapphire(data.pi,
                        breaks = breaks,
                        macrostate.annot = ifelse.c(pluck(opt, "npc", .default = -1) == 18,
                                                    macro_df,
                                                    NULL),
                        macrostate.pal = setNames(brewer.pal(6, "Set1")[c(1,2,3,6)], seq(4)),
                        breaks_number = F,
                        grain = pts2display,
                        kinet.max = 0.99,
                        kinet.transform = T,
                        kinet.who = "LocalCut",
                        dynam.col = dynam_col,
                        dynam.col.pal = replace(brain.pal, 1, "#fee391"),
                        dynam.size.val = 0.2,
                        dynam.special = puffs,
                        dynam.special.col = c("#e31a1c", "#1f78b4"),
                        dynam.special.size = rep(1.8, 2),
                        dynam.special.legend = "Time",
                        dynam.background = NULL, 
                        dynam.background.pal = NULL, 
                        dynam.background.alpha = 0.3,
                        addit.annot = add.ann,
                        addit.pal = add.pal,
                        ## addit.ylim = ylim_add,
                        discrete.annot = discr.ann,
                        discrete.pal = discr.pal,
                        discrete.trackline = list(Phase = track_rem),
                        discrete.trackline.pal = list(Phase = discr.pal$Phase[3]),
                        discrete.geom = "segment",
                        discrete.size = 0.03,
                        rescale.xaxis = F,
                        ratios = c(ifelse.c(pluck(opt, "npc", .default = -1) == 18, 0.24, NULL),
                                   1,
                                   1.3,
                                   rep(0.2, lt(add.ann)),
                                   rep(0.25, lt(discr.ann))
                                   ),
                        custom_theme = theme_tech("google", font = "sans", custom = T) +
                            theme(panel.background = element_blank(),
                                  text = element_text(color = "black")),
                        legend = list(discrete = c("Phase", "Behaviour"),
                                      addit = ifelse.c(str_detect(opt$set, "tc"),
                                                       c(TC = "TC", "Theta power" = "Theta power"),
                                                       c(Rate = "Rates"))),
                        legend.offset = c(0, 0.8),
                        legend.width = 0.11,
                        legend.rel_heights= c(1.6, 2.2, 1.3, 1.3, 1,3),
                        panel.grid.breaks = seq(1, nrow(data.pi), by = 250000),
                        plotlist = TRUE,
                        plotly = F,
                        silent = F,
                        big.size = 18,
                        medium.size = 16,
                        small.size = 10
                        )

    ## IMPORTANT Figure 4-4
    name_fig_barrier <- str_subset(list_fig, str_glue("{rat}-npc{opt$npc}-barrier"))
    ggsave(str_glue("{dir.plots}/{name_fig_barrier}.png"),
               plot=sapph$pl, width=17, height=14)
})


