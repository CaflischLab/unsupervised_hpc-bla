%% Function to compute wavelet; takes as input a .txt file

function archive_lfp_wavelet()
% 
% rat = 'Rat10';
% session = strcat(rat,'-20140628');
% anatom = 'bla_right';
% behav = 'postrun';

basepath = 'archive/';
dir_matlab = 'results/matlab/';

subsampl = 100 % subsampling in Hz

fileID = fopen('matlab_wavelet_args.txt');
args=textscan(fileID, '%s');
fclose(fileID);
rat = char(args{1}{1});
session = char(args{1}{2});
anatom = char(args{1}{3});
behav = char(args{1}{4});

disp(rat)
disp(session)
disp(anatom)
disp(behav)

if ~ismember(anatom, [string('bla_all'), string('bla_right'), string('bla_left'), string('hippo')]) 
    error('Anatomical part not well specified.')
end

if ~ismember(behav, [string('prerun'), string('presleep'), string('run'), string('postsleep'), string('postrun')]) 
    error('Anatomical part not well specified.')
end

dirInfo = strcat(basepath, rat, '/', rat, '-Info/');

nsessions = load(strcat(dirInfo, rat, '-nCell'));
nsessions = size(nsessions.nCellsRight, 1);

sessionList = listDirs(strcat(basepath, rat, '/', rat, '-20*'));


% Check if session is present in the session lists
if ~ismember(session, sessionList)
    error(strcat(session, ' session not found'))
end
session_num = find(contains(sessionList, session));

% Control on the channels 
param_gener = LoadParameters(strcat(dirInfo, rat, '.xml'));
channels = transpose(reshape(cell2mat(param_gener.spikeGroups.groups), [8, 20])); %Matrix 20*8 with shanks assignement to each shank

tmp = xml2struct(strcat(dirInfo, rat, '.xml'));
channels_descr = tmp.parameters.generalInfo.description.Text;

switch anatom
    case 'bla_all'
        struct_name = 'BLA';
        anatom_chann = (0:127);
        theta_band = [4 12];
        beta_band = [12 30];
        lowgamma_band = [30 70]; %Stujenske 2014
        highgamma_band = [70 120];
        ripples_band = [120 250];
    case 'bla_right'
        struct_name = 'BLA';
        anatom_chann = (0:63);
        theta_band = [4 12];
        beta_band = [12 30];
        lowgamma_band = [30 70]; %Stujenske 2014
        highgamma_band = [70 120];
        ripples_band = [120 250];
    case 'bla_left'
        struct_name = 'BLA';
        anatom_chann = (64:127);
        theta_band = [4 12];
        beta_band = [12 30];
        lowgamma_band = [30 70]; %Stujenske 2014
        highgamma_band = [70 120];
        ripples_band = [120 250];
    case 'hippo'
        struct_name = 'Hpc';
        anatom_chann = (128:159);
        theta_band = [4 14];
        beta_band = [14 30];
        lowgamma_band = [30 70];
        highgamma_band = [70 120];
        ripples_band = [120 250];
end

% *Assign and check the existence the channels relative to the input anatomical group*

tmp = load(strcat(dirInfo, 'Structures/', struct_name, '.mat'));

tmp = tmp.(struct_name);
tmp_idx = find(tmp(:,1)==session_num); %% NO channels/group meaning these indices
if isempty(tmp_idx) 
    error(strcat(anatom, ' not recorded in this session (it should be weird)'))        
end
groups_sel = transpose(tmp(tmp_idx, 2));
channels_sel = channels(groups_sel,:);

if ~any(ismember(channels_sel(:), anatom_chann))
     error(strcat(anatom, ' not recorded in this session (True check)'))
end

channels_sel = reshape(channels_sel.', 1, []);
channels_sel = channels_sel(ismember(channels_sel, anatom_chann));

% *Importing the session and check of the consistency*

cd(strcat(basepath, rat, '/', session));
xml_files = listFiles(strcat(basepath, rat, '/', session, '/*xml'));

% Check on presence of the correct xml file for settingg the session
if ~ismember(strcat(session, '.xml'), xml_files)
    error(strcat('xml file not present for this session ', join(xml_files)))
end

param_sess = LoadParameters(strcat(session, '.xml'));
% First Check on information consistency of the session
tmp = transpose(reshape((cell2mat(param_sess.spikeGroups.groups)), [8, 20]));
if ~isequal(tmp, channels)
    error('Channels on the shanks not corresponding for this session')
end

% Second Check on information consistency of the session
tmp= xml2struct(strcat(session, '.xml'));
if ~strcmp(tmp.parameters.generalInfo.description.Text, channels_descr)
    warning(strcat('data description of channels not corresponding'))
end


%% Importing the session without spikes
SetCurrentSession_optclu(strcat(session, '.xml'), false, true);

% *Timestamps of the events*

events_descr = GetEvents('output','descriptions');

max_time = max(GetEvents());
behav_descr = events_descr(contains(events_descr, strcat('-', behav)));
if length(behav_descr) < 2 
    error(strcat(string('Behaviour not present in the timestamp events (length ='), string(length(behav_descr))))
end
if mod(length(behav_descr),2) ~= 0 
    error(strcat(string('Odd number of timestamps'), string(length(behav_descr))))
end
behav_time = GetEvents(events_descr(contains(events_descr, strcat('-', behav))));


disp(strcat(string('Number of channels is  '), string(length(channels_sel))))



%% *LFP analysis*

bands_name={'delta'; 'theta'; 'spindles'; 'lowGamma'; 'highGamma'; 'ripples'};
bands_value=[0.5 4; theta_band; beta_band; lowgamma_band; highgamma_band; ripples_band];
windowSize = ceil(1250/subsampl); % 1250 Hz/ <n> Hz e.g. 20 Hz -> 50 ms
if mod(windowSize, 2) ~= 1; windowSize = windowSize - 1; end  % Window always odd  

power_tot = cell(length(channels_sel), 1);
phase_tot = cell(length(channels_sel), 1);

pause(5)

for i = 1:length(channels_sel)

    if mod(i,1)==0; disp(strcat(string('Doing '), string(i), string(' '), string(channels_sel(i)), string(' out of ' ), string(length(channels_sel)))); end

    [lfp, ~] = GetLFP(channels_sel(i), 'intervals', transpose(behav_time([1 end]))); %%AL

    indexes = 1:windowSize:size(lfp,1);
    if i == 1
        time = lfp(indexes,1);
    end

    power = zeros([size(lfp,1) length(bands_name)]);
    phase = zeros([size(lfp,1) length(bands_name)]);
    coi = zeros([size(lfp,1) length(bands_name)]);
    
    lfp = [lfp(:,1), locdetrend(lfp(:,2), 1250, [5 2])]; %% Tutorial Purpura 2008 on Chronux
   
%    
%         % Working with MATLAB 2016 
%         [tcw, ff, coi] = cwt(lfp(:,2), ...
%                                 'amor', ...
%                                 'NumOctaves', 16, ...
%                                 'VoicesPerOctave', 14, ...
%                                 1250);
%         tcw = flip(tcw);
%         ff = flip(ff);
%         tcw = tcw(ff > min(bands_value(:)) & ff < max(bands_value(:)), :);
%         ff = ff(ff > min(bands_value(:)) & ff < max(bands_value(:)));
%         [ffhist, ~, ~] = histcounts(ff, unique(bands_value(:)));
%         if i == 1; disp(ffhist); end
%         if any(ffhist < 10) 
%             error('Too little frequencies foung in the octave');
%         end
% 
%         for jj = 1:length(bands_value)
%             idxx = find( ff > bands_value(jj, 1) & ff < bands_value(jj, 2));
%             fftmp = ff(idxx);
%             if i == 1 
%                 disp(length(idxx)); disp(fftmp(1)); disp(fftmp(end));
%             end
%             power(:, jj) = trapz(fftmp, abs(tcw(idxx,:)).^2);
%             ww = diff([fftmp(1); (fftmp(:) + [diff(fftmp(:))/2; 0]) ]);
%             phase(:, jj)= cellfun(@(x) circ_mean(x, ww), num2cell(angle(tcw(idxx,:)), 1));
%         end
%         
%    

    % Working with MATLAB 2018 on for cwt 
    for jj = 1:length(bands_value)
        if i == 1; display(bands_name{jj}); end

        if jj  == 1
            bb = [ bands_value(jj,1)-0.1 bands_value(jj,2) + 0.1 ];
            oct = 6;
        elseif jj < 4
            bb = [ bands_value(jj,1)-0.1 bands_value(jj,2) + 0.1 ];
            oct = 12;
        elseif jj < 6
            bb = [ bands_value(jj,1)-1 bands_value(jj,2) + 0.1 ];
            oct = 20;
        else  
            bb = [ bands_value(jj,1)-2 bands_value(jj,2) + 0.1 ];
            oct = 18;
        end

        [tcw, ff, coi(:, jj)] = cwt(array2timetable(lfp(:,2), 'RowTimes', seconds(lfp(:,1))), ...
                                    'amor', ...
                                    'FrequencyLimits', bb, ...
                                    'VoicesPerOctave', oct);
        tcw = flip(tcw);
        ff = flip(ff);

        if i == 1; display(length(ff)); end
        if(length(ff) < 5) 
            error('Too little frequecies foung in the octave');
        end
        if i == 1; display([min(ff) max(ff)]); end
        %power(:, jj) = bandpower(abs(tcw).^2, ff, bands_value(jj,:), 'psd');
        power(:, jj) = trapz(ff, abs(tcw).^2);
        ww = diff([ff(1); (ff(:) + [diff(ff(:))/2; 0]) ]);
        phase(:, jj)= cellfun(@(x) circ_mean(x, ww), num2cell(angle(tcw), 1));
    end
        
    clear lfp tcw ww ff

    if any(isnan(power(:))); disp(string('contain NaN')); end

    disp('Computing power...')
    power = movmedian(power, windowSize, 1);
    power_tot{i} = cell2struct(num2cell(power(indexes,:), 1), bands_name, 2);
    disp('Computing phase...')
    phase = cellfun(@(x) circ_median(myreshape(x, windowSize, indexes)), ... 
                   num2cell(phase, 1), 'UniformOutput', false);
    phase_tot{i} = cell2struct(phase, bands_name, 2);

    if i == 1; coi_tot = coi(indexes,:); end

end

clear phase power


% *Operation over the channels*
power_avg = structfun(@(x) [], power_tot{1}, 'UniformOutput', false);
power_max = structfun(@(x) [], power_tot{1}, 'UniformOutput', false);
power_85q = structfun(@(x) [], power_tot{1}, 'UniformOutput', false);
for i = 1:length(bands_name)
   power_avg.(bands_name{i}) = mean(cell2mat(cellfun(@(x) x.(bands_name{i})', power_tot, 'UniformOutput', false)), 1);
   power_max.(bands_name{i}) = max(cell2mat(cellfun(@(x) x.(bands_name{i})', power_tot, 'UniformOutput', false)), [], 1);
   power_85q.(bands_name{i}) = quantile(cell2mat(cellfun(@(x) x.(bands_name{i})', power_tot , 'UniformOutput', false)), 0.85, 1);
end
power_avg.time = time';
power_max.time = time';
power_85q.time = time';

% *Operation over the channels*
warning('off', 'circ_median:ties');

phase_avg = structfun(@(x) [], phase_tot{1}, 'UniformOutput', false);
phase_med = structfun(@(x) [], phase_tot{1}, 'UniformOutput', false);
phase_std = structfun(@(x) [], phase_tot{1}, 'UniformOutput', false);
for i = 1:length(bands_name)
   phase_avg.(bands_name{i}) = circ_mean(cell2mat(cellfun(@(x) x.(bands_name{i})', phase_tot, 'UniformOutput', false)), [], 1);
   phase_med.(bands_name{i}) = circ_median(cell2mat(cellfun(@(x) x.(bands_name{i})', phase_tot, 'UniformOutput', false)), 1);
   phase_std.(bands_name{i}) = circ_std(cell2mat(cellfun(@(x) x.(bands_name{i})', phase_tot, 'UniformOutput', false)), [], [], 1);
end
phase_avg.time = time';
phase_med.time = time';
phase_std.time = time';

% *OUTPUT SECTION*
%   
name_file = strcat(session, '-', anatom, '-', behav); 

save(strcat(dir_matlab, name_file, '-lfp_powerwave_avg.mat'), 'power_avg')
save(strcat(dir_matlab, name_file, '-lfp_powerwave_max.mat'), 'power_max')
save(strcat(dir_matlab, name_file, '-lfp_powerwave_85q.mat'), 'power_85q')
disp(strcat(string('Saved '), name_file, string('-lfp_powerwave_xxx.mat')))

save(strcat(dir_matlab, name_file, '-lfp_phasewave_avg.mat'), 'phase_avg')
save(strcat(dir_matlab, name_file, '-lfp_phasewave_med.mat'), 'phase_med')
save(strcat(dir_matlab, name_file, '-lfp_phasewave_std.mat'), 'phase_std')
disp(strcat(string('Saved '), name_file, string('-lfp_phasewave_xxx.mat')))

save(strcat(dir_matlab, name_file, '-lfp_powerwave_coi.mat'), 'coi_tot')
disp(strcat(string('Saved '), name_file, string('-lfp_powerwave_coi.mat')))

disp('End of matlab script')

end


