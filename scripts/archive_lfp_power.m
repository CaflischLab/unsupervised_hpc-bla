%% matlab -nodisplay -nodesktop -r 'try archive_lfp_power(); catch ME; disp(strcat(" ERROR ::: ", ME.identifier)); end; quit'

function archive_lfp_power()

% rat = 'Rat10';
% session = strcat(rat,'Rat10-20140701');
% anatom = 'hippo';
% behav = 'presleep';


basepath = 'archive/'; %Archive directory hc-14
dir_matlab = 'results/matlab/'; %File output directory


%% Input given through the file matlab_args.txt
fileID = fopen('matlab_power_args.txt');
args=textscan(fileID, '%s');
fclose(fileID);
rat = char(args{1}{1});
session = char(args{1}{2});
anatom = char(args{1}{3});
behav = char(args{1}{4});


if ~ismember(anatom, ["bla_right", "bla_left", "hippo"]) 
    error('Anatomical part not well specified.')
end
if ~ismember(behav, ["prerun", "presleep", "run", "postsleep","postrun"]) 
    error('Anatomical part not well specified.')
end

dirInfo = strcat(basepath, rat, '/', rat, '-Info/');

if strcmp(session, 'Rat08-20130708') 
    error('This session is not to be analyzed.. the behaviour description is messy')
end

% Checks on session 
nsessions = load(strcat(dirInfo, rat, '-nCell'));
nsessions = size(nsessions.nCellsRight, 1);

sessionList = textscan(fopen(strcat(dirInfo, rat, '-SessionList.txt')),'%s');
sessionList = string(sessionList{1})';

if nsessions ~= length(sessionList)
    warning('MIsmatch between Number of sessions and rows of number of cells it is predictable if we are working with Rat11')
end

%% Check if session is present in the session lists
if ~ismember(session, sessionList)
    error(strcat(session, ' session not found'))
end
session_num = find(contains(sessionList, session));

%% Load the session parameters and electrode channels list

param_gener = LoadParameters(strcat(dirInfo, rat, '.xml'));
channels = transpose(reshape(cell2mat(param_gener.spikeGroups.groups), [8, 20])); %Matrix 20*8 with shanks assignement to each shank

tmp = xml2struct(strcat(dirInfo, rat, '.xml'));
channels_descr = tmp.parameters.generalInfo.description.Text;


% Options for the different anatomical parts
switch anatom
    case 'bla_all'
        struct_name = 'BLA';
        if strcmp(rat, 'Rat10') 
            anatom_chann = (0:127);
        elseif strcmp(rat, 'Rat08')
            anatom_chann = channels(5:20,:);
            anatom_chann = sort(anatom_chann(:));
        elseif strcmp(rat, 'Rat11')
            anatom_chann = channels(1:16,:);
            anatom_chann = sort(anatom_chann(:));
        end
        theta_band = [4 12];
        beta_band = [12 30];
        lowgamma_band = [30 70]; %Stujenske 2014
        highgamma_band = [70 120];
        ripples_band = [120 250];
    case 'bla_right'
        struct_name = 'BLA';
        if strcmp(rat, 'Rat10')
            anatom_chann = (0:63);
        elseif strcmp(rat, 'Rat08')
            anatom_chann = channels(13:20,:);
            anatom_chann = sort(anatom_chann(:));
        elseif strcmp(rat, 'Rat11')
            anatom_chann = channels(1:8,:);
            anatom_chann = sort(anatom_chann(:));
        end
        theta_band = [4 12];
        beta_band = [12 30];
        lowgamma_band = [30 70]; %Stujenske 2014
        highgamma_band = [70 120];
        ripples_band = [120 250];
    case 'bla_left'
        struct_name = 'BLA';
        if strcmp(rat, 'Rat10')
            anatom_chann = (64:127);
        elseif strcmp(rat, 'Rat08')
            anatom_chann = channels(5:12,:);
            anatom_chann = sort(anatom_chann(:));
        elseif strcmp(rat, 'Rat11')
            anatom_chann = channels(9:16,:);
            anatom_chann = sort(anatom_chann(:));
        end
        theta_band = [4 12];
        beta_band = [12 30];
        lowgamma_band = [30 70]; %Stujenske 2014
        highgamma_band = [70 120];
        ripples_band = [120 250];
    case 'hippo'
        struct_name = 'Hpc';
        if strcmp(rat, 'Rat10')
            anatom_chann = (128:159);
        elseif strcmp(rat, 'Rat08')
            anatom_chann = channels(1:4,:);
            anatom_chann = sort(anatom_chann(:));
        elseif strcmp(rat, 'Rat11')
            anatom_chann = channels(17:20,:);
            anatom_chann = sort(anatom_chann(:));
        end
        theta_band = [7 14];
        beta_band = [15 30];
        lowgamma_band = [30 70];
        highgamma_band = [70 120];
        ripples_band = [120 250];
end


%% Find the channels associated to the anatom; this step is
%% independent from the previous 
tmp = load(strcat(dirInfo, 'Structures/', struct_name, '.mat'));

tmp = tmp.(struct_name);
tmp_idx = find(tmp(:,1)==session_num); %% NO channels/group meaning these indices
if isempty(tmp_idx) 
    error(strcat(anatom, ' not recorded in this session (it should be weird)'))        
end
groups_sel = transpose(tmp(tmp_idx, 2));
channels_sel = channels(groups_sel,:);


%% Check if the information extracted on the channels match
%% the one for that specific anatomical part 
if ~any(ismember(channels_sel(:), anatom_chann))
    error(strcat(anatom, ' not recorded in this session (True check)'))
end

channels_sel = reshape(channels_sel.', 1, []);
channels_sel = channels_sel(ismember(channels_sel, anatom_chann));


%% Importing again the session parameters and check of the consistency

cd(strcat(basepath,rat,'/',session));
xml_files = listFiles(strcat(basepath,rat,'/',session, '/*xml'));

%% First Check on information consistency of the session
param_sess = LoadParameters(strcat(session, '.xml'));
tmp = transpose(reshape((cell2mat(param_sess.spikeGroups.groups)), [8, 20]));
if ~isequal(tmp, channels)
    error('Channels on the shanks not corresponding for this session')
end

%% Second Check on information consistency of the session
tmp= xml2struct(strcat(session, '.xml'));
if ~strcmp(tmp.parameters.generalInfo.description.Text, channels_descr)
    warning(strcat('data description of channels not corresponding'))
end

%% Eventually setting session

SetCurrentSession_optclu(strcat(session, '.xml'), false, true);

%% Timestamps of the events

max_time = max(GetEvents());
events_descr = GetEvents('output','descriptions');

%% Getting the timestamps of the different epochs

behav_descr = events_descr(contains(events_descr, strcat('-', behav)))

if length(behav_descr) < 2 
    error(strcat("Behaviour not present in the timestamp events length ", string(length(behav_descr))))
end

if mod(length(behav_descr),2) ~= 0 
    error(strcat("Odd number of timestamps ", string(length(behav_descr))))
end

behav_time = GetEvents(events_descr(contains(events_descr, strcat('-', behav))));


%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

disp(strcat("Number of channels is  ", string(length(channels_sel))))

%% LFP analysis

warning off;

lfp_specgr = cell(length(channels_sel), 1);
bands = cell(length(channels_sel), 1);

for i = 1:length(channels_sel)
    for ll = 1:length(behav_time)/2
        [tmp, ~] = GetLFP(channels_sel(i), 'intervals', transpose(behav_time(2*ll-1:2*ll))); %%ALreadyt filtered up to circa 500Hz (Nyquist range)
        if ll==1 
            lfp = tmp;
        else 
            lfp = vertcat(lfp, tmp);
        end
    end
    clear tmp;
    
    lfp = [lfp(:,1), locdetrend(lfp(:,2), 1250, [0.5 0.1])]; %% Tutorial Purpura 2008 on Chronux
    lfp = FilterLFP(lfp, 'passband', [0 250]); %%Range from Kelly 2010

    if i==1
        [lfp_specgr{i}, t_specgr, f_specgr] = ...
        MTSpectrogram(lfp, 'range', [0 250], 'window', 2, 'step',  0.05, 'tapers', [3 5], 'show', 'off');
    else
        [lfp_specgr{i}, ~, ~] = ...
        MTSpectrogram(lfp, 'range', [0 250], 'window', 2, 'step',  0.05, 'tapers', [3 5], 'show', 'off');
    end
    if mod(i,2)==0; disp(strcat(string(i), "  " ,string(channels_sel(i)))); end
    
    bands{i} = SpectrogramBands(lfp_specgr{i}, f_specgr, 'delta', [0.3 4], 'theta', theta_band, 'spindles', beta_band, 'lowGamma', lowgamma_band, 'highGamma', highgamma_band, 'ripples', ripples_band);
    clear lfp                          
end


%% Summary Operations over the channels -> mean, max, 85% percentile
bands_name = fieldnames(bands{1});
bands_name = bands_name(bands_name~="ratios");
bands_85q = structfun(@(x) [], bands{1}, 'UniformOutput', false);
for i = 1:length(bands_name)
   tmp = cell2mat(cellfun(@(x) transpose(x.(bands_name{i})), bands, 'UniformOutput', false));
   tmp = transpose(num2cell(tmp, 1));
   bands_85q.(bands_name{i}) = transpose(cellfun(@(x) quantile(x, 0.85), tmp));
end


%
bands_name_ratios = fieldnames(bands{1}.ratios);
bands_85q.ratios = structfun(@(x) [], bands{1}.ratios, 'UniformOutput', false);
for i = 1:length(bands_name_ratios)
   tmp = cell2mat(cellfun(@(x) transpose(x.ratios.(bands_name_ratios{i})), bands, 'UniformOutput', false));
   tmp = transpose(num2cell(tmp, 1));
   bands_85q.ratios.(bands_name_ratios{i}) = transpose(cellfun(@(x) quantile(x, 0.85), tmp));
end

bands_85q.time = transpose(t_specgr);


if ~all([1 length(t_specgr)] == transpose(unique(structfun(@(x) size(x, 2), bands_85q, 'UniformOutput', true))))
    error("Problem in band construction");
end
if length(t_specgr) ~= unique(structfun(@(x) size(x, 2), bands_85q.ratios, 'UniformOutput', true)) 
    error("Problem in band construction");
end


%% Saving the power bands
%   
name_file = strcat(session, '-', anatom, '-', behav); 
save(strcat(dir_matlab, name_file, '-lfp_win2s_85q.mat'), 'bands_85q')
disp(strcat("Saved ", name_file, "-lfp_win2s_85q.mat"))


%% REM NREM analysis

if ismember(behav, ["presleep" "postsleep"])    

    %% Values and settings taken from the Girardeau script 
    pos = GetPositions('coordinates', 'real', 'pixel', 0.43, 'discard', 'partial');
    if any(isnan(pos(:)))
        error('NaN values in positions')
    end

    %% *Check if the signal has been disrupted for too much time*
    gaps_vel = diff([0; pos(:,1)]);
    if(any(gaps_vel > 30)) 
        warning(strcat(string(sum(gaps_vel > 30)), ' gaps between recorded position bigger than 30s. Max value is ', string(max(gaps_vel))))
    end

    velocity_led1 = LinearVelocity(pos(:,1:3));
    velocity_led2 = LinearVelocity(pos(:, [1 4:5]));

    velocity_mean = [velocity_led1(:,1) mean([velocity_led1(:,2) velocity_led2(:,2)], 2)];
    [quiet_pairs, quiet_mask] = QuietPeriods(velocity_mean, 3, 30, 0.5); %% Parameters taken from Maingret 2016

    brain_st = struct('awake', [], 'sws', [], 'rem', []);
    switch anatom
      case 'hippo'
        brain_st_meth = 'hippocampus';
      otherwise
        brain_st_meth = 'amygdala';
    end
    for i = 1:length(channels_sel)
        [brain_st.awake(:,i), brain_st.sws(:,i), brain_st.rem(:,i)] = ...
            BrainStates(lfp_specgr{i}, t_specgr, f_specgr, quiet_mask, [], 'method', brain_st_meth, 'show', 'none');
    end

    save(strcat(dir_matlab, name_file, '-win2s-brain_states.mat'), 'brain_st')
    disp(strcat("Saved ", name_file, "-win2s-brain_states.mat"))
end

disp('End of matlab script')
disp('-----------------------------------------------------------')
disp("   ")






