norm.diff <- function(x, y) {
    return(x - y)
}

create.scores.normalized <- function(vars,
                                     levels,
                                     inWhat,
                                     globalFilter,
                                     slicing = NULL,
                                     slicing.name = NULL,
                                     ...,
                                     min.seconds,
                                     session.list,
                                     shuffle.tests = F,
                                     dir.input = "./"
                                     dir.output = "./")  {
    
    additionalFilter <- enquos(...)
    if(lt(vars) != 2) stop("vars must be lenght 2")
    if(lt(levels) != 2) stop("levels must be lenght 2")
    
    var1 <- vars[1]
    var2 <- vars[2]
    levels1 <- levels[[1]]
    levels2 <- levels[[2]]

    globalFilter <- enquo(globalFilter)
    print(globalFilter)
    rat <- str_extract(session.list, "Rat[0-9]{2}") %>% unique() %>% str_c(collapse = "_")
    root.name <- str_glue("{dir.output}{rat}-bla_right-win2s-power-G_{paste0(inWhat, collapse=\"_\")}-L1_{paste0(levels1, collapse=\"_\")}-L2_{paste0(levels2, collapse=\"_\")}")

    if(any(vars == "direction")) {
        nm <- levels[[which(vars == "direction")]]
        root.name <- str_replace(root.name, str_c(nm, collapse = "_"), str_c("dir", nm, collapse = "_"))
    }
    
    if(!is.null(slicing)) {
        if(is.null(slicing.name)) stop("Provide slicing name")
        root.name <- str_glue("{root.name}-{slicing.name}")
    }

    summ <- map.c(session.list, function(sess) {

        cat(sess, "\n")
        ## ####################################
        ## This part does not depend on the variable to analyze
        tcs <- readRDS(str_glue("{dir.input}{sess}-bla_right-power-topoc_dataframe.RData"))
        lts <- map(tcs, ~ min(sapply(.x, nrow)))
        tcs <- map2(tcs, lts, ~ map(.x, tail, .y)) %>% 
            modify_depth(2, ~ select(.x, matches("(tc[1-2]{2}|alpha)")) %>%
                                rename_all(str_replace, "pcorr.", "")) %>%
            map(~ unlist(.x, recursive=F) %>% bind_cols() %>% rename_all(split.and.rev, "."))
        lab <- readRDS(str_glue("{dir.input}{sess}-bla_right-win2s-labels_dataframe.RData")) %>%
            map2(lts, ~ tail(.x, .y)) 
        tot <- map2_dfr(lab, tcs, bind_cols, .id = 'behav') %>%
            mutate(behav = factor(behav, levels = behav.list))
        rm(lab, tcs)

        ## ####################################
        ## Applying the various filters

        summ <- tot %>%
            mutate(rpp = factor(rpp, levels = c(0,1), labels=c("Norpp", "rpp"))) %>%
            filter(!!as.name(var1) %in% levels1) %>%
            filter(!!as.name(var2) %in% levels2) %>%
            filter(!!globalFilter) %>%
            droplevels() 

        if(lt(additionalFilter) != 0) summ <- filter(summ, !!!additionalFilter) %>%
                                          droplevels()
        if(!is.null(slicing)) {
            summ <- slicing(summ)
        }
        
        if(nrow(summ) == 0) stop("No survival after selection...")

        ## Check if the minimum number of seconds, within each session, is fulfilled;
        ## if not remove the session 
        test <- summ %>%
            group_by(!!as.name(var1), !!as.name(var2)) %>%
            summarize(count = n()*0.05) %>%
            pull(count)
        
        if(any(test < min.seconds)) return(NULL)
        

        ## Summarizing
        summ <- summ %>%
            group_by(!!as.name(var1), !!as.name(var2)) %>%
            select(!!as.name(var1), !!as.name(var2), matches("^(tc[1-2]{2}|alpha)")) %>%
            add_count(name = "counts") %>%
            summarize_all(mean) %>%
            rename(filter1 = var1, filter2 = var2) %>%
            ungroup() %>%
            unite(filter1, filter2, col="join", sep=".")

        return(summ)
    }) %>% bind_rows(.id = "session")

    if(data.table::uniqueN(summ$session) < lt(session.list)) {
        cat("Sessions higher than min.secs are", data.table::uniqueN(summ$session), "\n")
    }
    
    ## Remove sessions not complete
    uncomplete <- group_by(summ, session) %>% summarize(n = n()) %>%
        filter(n != 4) %>% pull(session)
    if(lt(uncomplete) > 0) {
        cat("Sessions uncomplete are", lt(uncomplete), "\n")
        summ <- filter(summ, ! session %in% uncomplete)
    }

    if(data.table::uniqueN(summ$session) < 10) {
        stop("Less than 10 sessions complete....")
    }

    
    summ.mean <- summ %>%
        gather(key = "feat", value="value", matches("(tc[1-2]{2}|alpha)")) %>%
        separate(feat, into = c("mat", "bb"), sep = "\\.") %>%
        separate(bb, into = c("HPC", "BLA"), sep = "_")

    ## ############################################
    ## ## Saving point
    
    saveRDS(summ.mean, str_glue("{root.name}-tcs_scores_meanvalues.RData"))
    cat("Saved", str_glue("{root.name}-tcs_scores_meanvalues.RData"), "\n")


    ## ############################################
    ## Averaging over the sessions, i.e. the actual values displayed in the plots
    
    summ.mean <- summ.mean %>%
        group_by(mat, join, HPC, BLA) %>%
        select(value) %>%
        summarize_all(mean) %>%
        ungroup() %>%
        group_by(mat) %>%
        nest.c() %>%
        map(~ group_by(.x, join) %>% nest.c())

    ## ############################################
    ## Computing the differences, i.e. side panels

    all.diff <- list(hor = list(var = var1, lev = levels1),
                     ver = list(var = var2, lev = levels2)) %>%
        map.c(function(ls) {
            var <- ls$var
            lev <- ls$lev
            var.other <- setdiff(c(var1, var2), var)
            lev.diff <- paste0(rev(lev), collapse="_")
            diff <- summ %>%
                separate(join, into = c(var1, var2), sep = "\\.") %>%
                group_by(!!as.name(var.other)) %>% ## Grouping by the vertical
                select(-counts) %>% ## Crucial removing this feature for the next step of tidyr::spread
                gather(key = "feat", value="value", matches("(tc[1-2]{2}|alpha)")) %>%
                spread(key = !!as.name(var), value = value) %>%
                mutate(!!lev.diff := norm.diff(!!as.name(lev[2]), !!as.name(lev[1]))) %>%
                select(-!!as.name(lev[1]), -!!as.name(lev[2])) %>%
                nest.c() %>%
                map(~ separate(.x, feat, into = c("mat", "bb"), sep = "\\.") %>%
                        separate(bb, into = c("HPC", "BLA"), sep = "_") %>%
                        group_by(mat) %>%
                        nest.c()) %>%
                transpose()
            return(diff)
        })

    summ.diff <- map_depth(all.diff, 3, ~ group_by(.x, HPC, BLA) %>% ## Averaging over session
                                            summarize_if(is.numeric, mean) %>%
                                            ungroup()) 

    ## ##########################################################
    ## ##########################################################
    ## Statistical tests
    
    comb <- cross(levels) %>%
        map(paste0, collapse = ".") %>%
        unlist() %>%
        outer(., ., "paste", sep="_") %>%
        `[`(lower.tri(.))

    ## ################################################### 
    ## Wilcoxon test
    
    test.wilcox <- map.c(comb, function(cc) {
        dft <- as.vector(str_split_fixed(cc, "_", 2))
        cc1 <- filter(summ, join == dft[1]) %>% select(matches("(tc[1-2]{2}|alpha)"))
        cc2 <- filter(summ, join == dft[2]) %>% select(matches("(tc[1-2]{2}|alpha)"))
        out <- map2(cc1, cc2, function(t1, t2) {
            ## stackoverflow.com/questions/45233658/meaning-v-value-wilcoxen-signed-rank-test
            v.stat <- sum(rank(abs((t1-t2)[which(t1 - t2 != 0)]))[(t1-t2)[which(t1 - t2 != 0)] > 0])
            w.stat <- sum(sign(t2-t1)[which(t2-t1 != 0)]*rank(abs(t2-t1)[which(t2-t1 != 0)]))
            tmp <- wilcox.test(x=t1, y=t2, paired=TRUE)
            if(v.stat != tmp$statistic) browser()
            ## tmp <- t.test(t1, t2, alternative="two.sided", var.equal=FALSE) ## i.e Welch's test
            list(value = tmp$p.value, v.stat = as.vector(tmp$statistic), w.stat = w.stat)
        })
        bind_rows(out, .id = "feat") %>%
            separate(feat, into = c("mat", "bb"), sep = "\\.") %>%
            separate(bb, into = c("HPC", "BLA"), sep = "_") %>%
            group_by(mat) %>%
            nest.c()
    }) %>% transpose()

    test.onetail <- map.c(comb, function(cc) {
        dft <- as.vector(str_split_fixed(cc, "_", 2))
        cc1 <- filter(summ, join == dft[1]) %>% select(matches("(tc[1-2]{2}|alpha)"))
        cc2 <- filter(summ, join == dft[2]) %>% select(matches("(tc[1-2]{2}|alpha)"))
        out <- map2(cc1, cc2, function(t1, t2) {
            tmp <- map_dbl(c.c("less", "greater"),
                           ~ wilcox.test(x=t1, y=t2, paired=TRUE, alternative = .x)$p.value)
            enframe(tmp[which.min(tmp)], name = "tail", value = "value")
        })
        pval <- bind_rows(out, .id = "feat") %>%
            separate(feat, into = c("mat", "bb"), sep = "\\.") %>%
            separate(bb, into = c("HPC", "BLA"), sep = "_") %>%
            group_by(mat) %>%
            nest.c()
    }) %>% transpose()

    

    
    ## ################################################### 
    ## First Naive Shuffling test ## INCOMPLETE

    if(shuffle.tests) {
        
        test.normalks <- NULL

    }

    
    ## ##########################################################
    ## ##########################################################
    ## Plot features

    counts <- summ %>%
        select(session, join, counts) %>%
        group_by(join) %>%
        summarize_at("counts", sum) %>%
        spread(join, counts)
    
    ttl <- str_glue("bla_right \n GLOB {paste0(inWhat,collapse=\"_\")}")
    if(lt(additionalFilter) != 0) {
        ttl2 <- map_chr(additionalFilter, rlang::quo_name) %>% paste(collapse = " | ")
        ttl <- str_glue("{ttl}\n{ttl2}")
    }
    
    ## ######################################
    ## Saving point

    if(shuffle.tests) {
        setup <- list(mean = summ.mean,
                      test.wilcox = test.wilcox,
                      test.onetail = test.onetail,
                      test.normalks = test.normalks,
                      diff.hor = summ.diff$hor, diff.ver= summ.diff$ver) %>%
            map(~ .x[c("tc12", "tc21")])
    } else {
        setup <- list(mean = summ.mean,
                      test.wilcox = test.wilcox,
                      test.onetail = test.onetail,
                      diff.hor = summ.diff$hor, diff.ver= summ.diff$ver) %>%
            map(~ .x[c("tc12", "tc21")])
    }

    ## IMPORTANT:: Actual files used in the interaction tables plots
    saveRDS(splice(setup, order1 = levels1, order2 = levels2, main =ttl),
            str_glue("{root.name}-tcs_scores_setup_normalized.RData"))

    ## ######################################
    ## ######################################
    ## PLOTS

    out <- map.c(c("wilcox", "onetail", "normalks"), function(test.type) {

        ## #####
        ## Triangles plot with TC12 and TC21 together
        if(!shuffle.tests & ! test.type %in%  c("wilcox", "onetail") ) {
            return(NULL)
        }
        
        pl.new <- plot.scores.4panels.triangle(setup,
                                               order1 = levels1, order2 = levels2,
                                               main = ttl,
                                               test.type = test.type,
                                               rel_width = c(0.5, 2.35, 1.40),
                                               rel_height = c(2.05, 1.15))

        ggsave(filename = str_glue("{root.name}-tcs_scores_{test.type}_normalized.png"),
               pl.new$tot,
               width = 10.5, height = 8.7, units = 'in', limitsize = T)
        cat("Saved", root.name, ".png & .RData\n")

        plot.classic <- NULL
        
        return(list(plot.classic = plot.classic, plot.new = pl.new))
    })

    invisible(out)
}




## BASICALLY THE SAME AS ABOVE. Just another definition of Sleep State from Hpc.
## TO bE merged
rebuttal_create.scores.normalized <- function(vars,
                                              levels,
                                              inWhat,
                                              globalFilter,
                                              slicing = NULL,
                                              slicing.name = NULL,
                                              ...,
                                              min.seconds,
                                              session.list,
                                              only_mean = F,
                                              dir.input = "./"
                                              dir.output = "./")  {

    
    additionalFilter <- enquos(...)
    if(lt(vars) != 2) stop("vars must be lenght 2")
    if(lt(levels) != 2) stop("levels must be lenght 2")
    
    var1 <- vars[1]
    var2 <- vars[2]
    levels1 <- levels[[1]]
    levels2 <- levels[[2]]

    globalFilter <- enquo(globalFilter)
    print(globalFilter)
    rat <- str_extract(session.list, "Rat[0-9]{2}") %>% unique() %>% str_c(collapse = "_")
    root.name <- str_glue("{dir.output}{rat}-bla_right-win2s-power-G_{paste0(inWhat, collapse=\"_\")}-L1_{paste0(levels1, collapse=\"_\")}-L2_{paste0(levels2, collapse=\"_\")}")

    cat(root.name, "\n")
    
    if(any(vars == "direction")) {
        nm <- levels[[which(vars == "direction")]]
        root.name <- str_replace(root.name, str_c(nm, collapse = "_"), str_c("dir", nm, collapse = "_"))
    }
    
    if(!is.null(slicing)) {
        if(is.null(slicing.name)) stop("Provide slicing name")
        root.name <- str_glue("{root.name}-{slicing.name}")
    }

    summ <- map.c(session.list, function(sess) {

        cat(sess, "\n")
        ## ####################################
        ## This part does not depend on the variable to analyze
        tcs <- readRDS(str_glue("{dir.input}{sess}-bla_right-power-topoc_dataframe.RData"))
        lts <- map(tcs, ~ min(sapply(.x, nrow)))
        tcs <- map2(tcs, lts, ~ map(.x, tail, .y)) %>% 
            modify_depth(2, ~ select(.x, matches("(tc[1-2]{2}|alpha)")) %>%
                                rename_all(str_replace, "pcorr.", "")) %>%
            map(~ unlist(.x, recursive=F) %>% bind_cols() %>% rename_all(split.and.rev, "."))

        lab <- readRDS(str_glue("{dir.input}{sess}-bla_right-win2s-labels_dataframe.RData")) %>%
            map2(lts, ~ tail(.x, .y))

        
        summ <- map2_dfr(lab, tcs, bind_cols, .id = 'behav') %>%
            mutate(behav = factor(behav, levels = behav.list))
        rm(lab, tcs)


        ## #################
        ## Rebuttal correction
        ## Substitute the brain state extracted from Hpc activity
        ## Substitute the other brain state BEFORE THE FILTERS
        cat("Using the state definition of the Hippocampus\n")

        state_ofhpc <- read.brain.states(anatom = "bla_right",
                                         session = sess,
                                         type = "hpc",
                                         lfp.type = "win2s",
                                         dir.analysis = dir.input,
                                         dir.matlab = dir.matlab)[[1]] %>%
            `[`(c("presleep", "postsleep")) %>%
            bind_rows(.id = "behav") %>%
            as_tibble() %>%
            mutate_at("bin", factor, levels=seq(3), labels= c("awake", 'sws', "rem")) %>%
            rename("state_hpc" = bin)

        summ <- as_tibble(summ) %>%
            left_join(state_ofhpc, by = c("behav", "time")) %>%
            relocate("state_hpc", .after = "state") %>%
            select(-state) %>%
            rename("state" = state_hpc)

        rm(state_ofhpc)

        ## ######################
        ## Applying the filters

        summ <- as_tibble(summ) %>%
            mutate(rpp = factor(rpp, levels = c(0,1), labels=c("Norpp", "rpp"))) %>%
            filter(across(all_of(var1), ~ . %in% levels1)) %>%
            filter(across(all_of(var2), ~ . %in% levels2)) %>%
            filter(!!globalFilter) %>%
            droplevels() 

        if(lt(additionalFilter) != 0) {
            summ <- filter(summ, !!!additionalFilter) %>%
                droplevels()
        }
        if(!is.null(slicing)) {
            summ <- slicing(summ)
        }
        if(nrow(summ) == 0) stop("No survival after selection...")

        
        ## Check if the minimum number of seconds, within each session, is fulfilled;
        ## if not remove the session
        
        test <- summ %>%
            group_by(across(all_of(c(var1, var2)))) %>%
            summarize(count = n()*0.05, .groups = "drop") %>%
            pull(count)
        
        if(any(test < min.seconds)) return(NULL)

        ## Summarizing
        summ <- summ %>%
            group_by(across(all_of(c(var1, var2)))) %>%
            select(all_of(c(var1, var2)), matches("^(tc[1-2]{2}|alpha)")) %>%
            add_count(name = "counts") %>%
            summarize_all(mean) %>%
            rename(filter1 = var1, filter2 = var2) %>%
            ungroup() %>%
            unite(filter1, filter2, col="join", sep=".")

        return(summ)
    }) %>% bind_rows(.id = "session")

    if(data.table::uniqueN(summ$session) < lt(session.list)) {
        cat("Sessions higher than min.secs are", data.table::uniqueN(summ$session), "\n")
    }
    
    ## Remove sessions not complete
    incomplete <- group_by(summ, session) %>%
        summarize(n = n(), .groups = "drop") %>%
        filter(n != 4) %>%
        pull(session)
    if(lt(incomplete) > 0) {
        cat("Sessions incomplete are", lt(incomplete), "\n")
        summ <- filter(summ, ! session %in% incomplete)
    }

    if(n_distinct(summ$session) < ifelse.c(n_distinct(str_extract(session.list, "Rat[0-9]{2}")) == 1, 3, 10)) {
        stop("Less than 3 (or 10) sessions complete....")
    }

    summ.mean <- summ %>%
        gather(key = "feat", value="value", matches("(tc[1-2]{2}|alpha)")) %>%
        separate(feat, into = c("mat", "bb"), sep = "\\.") %>%
        separate(bb, into = c("HPC", "BLA"), sep = "_")

    if(only_mean) {
        cat("Returning mean values\n")
        return(summ.mean)
    }
    
    ## ############################################
    ## ## Saving point
    
    saveRDS(summ.mean, str_glue("{root.name}-tcs_scores_meanvalues.RData"))
    cat("Saved", str_glue("{root.name}-tcs_scores_meanvalues.RData"), "\n")

    ## ############################################
    ## Averaging over the sessions, i.e. the actual values displayed in the plots
    
    summ.mean <- summ.mean %>%
        select(mat, join, HPC, BLA, value) %>%
        group_by(across(-value)) %>%
        summarize_all(mean, .groups = "drop") %>%
        ungroup() %>%
        nest(tmp = -mat) %>%
        mutate_at("tmp", map, ~ nest(.x, tmp2 = -join) %>% deframe) %>%
        deframe()
    
    ## ############################################
    ## Computing the differences, i.e. side panels

    all.diff <- list(hor = list(var = var1, lev = levels1),
                      ver = list(var = var2, lev = levels2)) %>%
        map.c(function(ls) {
            var <- ls$var
            lev <- ls$lev
            var.other <- setdiff(c(var1, var2), var)
            lev.diff <- paste0(rev(lev), collapse="_")
            diff <- summ %>%
                separate(join, into = c(var1, var2), sep = "\\.") %>%
                group_by(!!as.name(var.other)) %>% ## Grouping by the vertical
                select(-counts) %>% ## Crucial removing this feature for the next step of tidyr::spread
                gather(key = "feat", value="value", matches("(tc[1-2]{2}|alpha)")) %>%
                spread(key = !!as.name(var), value = value) %>%
                mutate(!!lev.diff := norm.diff(!!as.name(lev[2]), !!as.name(lev[1]))) %>%
                select(-!!as.name(lev[1]), -!!as.name(lev[2])) %>%
                nest.c() %>%
                map(~ separate(.x, feat, into = c("mat", "bb"), sep = "\\.") %>%
                        separate(bb, into = c("HPC", "BLA"), sep = "_") %>%
                        group_by(mat) %>%
                        nest.c()) %>%
                transpose()
            return(diff)
        })

    summ.diff <- map_depth(all.diff, 3, ~ group_by(.x, HPC, BLA) %>% ## Averaging over session
                                            summarize_if(is.numeric, mean) %>%
                                            ungroup()) 

    ## ##########################################################
    ## ##########################################################
    ## Statistical tests
    
    comb <- cross(levels) %>%
        map(paste0, collapse = ".") %>%
        unlist() %>%
        outer(., ., "paste", sep="_") %>%
        `[`(lower.tri(.))

    ## ################################################### 
    ## Wilcoxon test
    
    test.wilcox <- map.c(comb, function(cc) {
        dft <- as.vector(str_split_fixed(cc, "_", 2))
        cc1 <- filter(summ, join == dft[1]) %>% select(matches("(tc[1-2]{2}|alpha)"))
        cc2 <- filter(summ, join == dft[2]) %>% select(matches("(tc[1-2]{2}|alpha)"))
        out <- map2(cc1, cc2, function(t1, t2) {
            ## stackoverflow.com/questions/45233658/meaning-v-value-wilcoxen-signed-rank-test
            v.stat <- sum(rank(abs((t1-t2)[which(t1 - t2 != 0)]))[(t1-t2)[which(t1 - t2 != 0)] > 0])
            w.stat <- sum(sign(t2-t1)[which(t2-t1 != 0)]*rank(abs(t2-t1)[which(t2-t1 != 0)]))
            tmp <- wilcox.test(x=t1, y=t2, paired=TRUE)
            if(v.stat != tmp$statistic) browser()
            ## tmp <- t.test(t1, t2, alternative="two.sided", var.equal=FALSE) ## i.e Welch's test
            list(value = tmp$p.value, v.stat = as.vector(tmp$statistic), w.stat = w.stat)
        })
        bind_rows(out, .id = "feat") %>%
            separate(feat, into = c("mat", "bb"), sep = "\\.") %>%
            separate(bb, into = c("HPC", "BLA"), sep = "_") %>%
            group_by(mat) %>%
            nest.c()
    }) %>% transpose()

    test.onetail <- map.c(comb, function(cc) {
        dft <- as.vector(str_split_fixed(cc, "_", 2))
        cc1 <- filter(summ, join == dft[1]) %>% select(matches("(tc[1-2]{2}|alpha)"))
        cc2 <- filter(summ, join == dft[2]) %>% select(matches("(tc[1-2]{2}|alpha)"))
        out <- map2(cc1, cc2, function(t1, t2) {
            tmp <- map_dbl(c.c("less", "greater"),
                           ~ wilcox.test(x=t1, y=t2, paired=TRUE, alternative = .x)$p.value)
            enframe(tmp[which.min(tmp)], name = "tail", value = "value")
        })
        pval <- bind_rows(out, .id = "feat") %>%
            separate(feat, into = c("mat", "bb"), sep = "\\.") %>%
            separate(bb, into = c("HPC", "BLA"), sep = "_") %>%
            group_by(mat) %>%
            nest.c()
    }) %>% transpose()

    
    ## USELESS
    counts <- summ %>%
        select(session, join, counts) %>%
        group_by(join) %>%
        summarize_at("counts", sum) %>%
        spread(join, counts)
    
    ## ##########################################################
    ## Title

    ttl <- str_glue("bla_right \n GLOB {paste0(inWhat,collapse=\"_\")}")
    if(lt(additionalFilter) != 0) {
        ttl2 <- map_chr(additionalFilter, rlang::quo_name) %>% paste(collapse = " | ")
        ttl <- str_glue("{ttl}\n{ttl2}")
    }
    
    ## ######################################
    ## Saving point

    setup <- list(mean = summ.mean,
                      test.wilcox = test.wilcox,
                      test.onetail = test.onetail,
                      diff.hor = summ.diff$hor, diff.ver= summ.diff$ver) %>%
        map(~ .x[c("tc12", "tc21")])
    out <- splice(setup, order1 = levels1, order2 = levels2, main =ttl)
    
    ## IMPORTANT:: Actual files used in the interaction tables plots
    saveRDS(out,
            str_glue("{root.name}-tcs_scores_setup_normalized.RData"))

    invisible(out)
}




create.scores.random.diff <- function(vars,
                                      levels,
                                      inWhat,
                                      globalFilter,
                                      ...,
                                      session.list,
                                      nrand = 1000,
                                      dir.input = "./"
                                      dir.output = "./")  {
    

    additionalFilter <- enquos(...)
    if(lt(vars) != 2) stop("vars must be lenght 2")
    if(lt(levels) != 2) stop("levels must be lenght 2")
    
    var1 <- vars[1]
    var2 <- vars[2]
    levels1 <- levels[[1]]
    levels2 <- levels[[2]]

    globalFilter <- enquo(globalFilter)
    print(globalFilter)

    rat <- str_extract(session.list, "Rat[0-9]{2}") %>% unique() %>% str_c(collapse = "_")
    root.name <- str_glue("{dir.output}{rat}-bla_right-win2s-power-G_{paste0(inWhat, collapse=\"_\")}-L1_{paste0(levels1, collapse=\"_\")}-L2_{paste0(levels2, collapse=\"_\")}")
    if(any(vars == "direction")) {
        nm <- levels[[which(vars == "direction")]]
        root.name <- str_replace(root.name, str_c(nm, collapse = "_"), str_c("dir", nm, collapse = "_"))
    }

    
    summ <- map.c(session.list, function(sess) {

        cat(sess, "\n")
        ## ####################################
        ## This part does not depend on the variable to analyze
        tcs <- readRDS(str_glue("{dir.input}{sess}-bla_right-power-topoc_dataframe.RData"))
        lts <- map(tcs, ~ min(sapply(.x, nrow)))
        tcs <- map2(tcs, lts, ~ map(.x, tail, .y)) %>% 
            modify_depth(2, ~ select(.x, matches("(tc[1-2]{2}|alpha)")) %>%
                                rename_all(str_replace, "pcorr.", "")) %>%
            map(~ unlist(.x, recursive=F) %>% bind_cols() %>% rename_all(split.and.rev, "."))
        lab <- readRDS(str_glue("{dir.input}{sess}-bla_right-win2s-labels_dataframe.RData")) %>%
            map2(lts, ~ tail(.x, .y)) 
        tot <- map2_dfr(lab, tcs, bind_cols, .id = 'behav') %>%
            mutate(behav = factor(behav, levels = behav.list))
        rm(lab, tcs)

        ## ####################################
        ## Applying the various filters

        summ <- tot %>%
            mutate(rpp = factor(rpp, levels = c(0,1), labels=c("Norpp", "rpp"))) %>%
            filter(!!as.name(var1) %in% levels1) %>%
            filter(!!as.name(var2) %in% levels2) %>%
            filter(!!globalFilter) %>%
            droplevels() ## "behav"

        if(lt(additionalFilter) != 0) summ <- filter(summ, !!!additionalFilter) %>%
                                          droplevels()
        
        if(nrow(summ) == 0) stop("No survival after selection...")

        ## Randomize both the levels keeping the pairing
        summ.paired <- map_dfr(seq(nrand), function(ii) {
            ## In this way var1 and var2 are randomized by keeping the pairing
            tmp.seq <- sample(nrow(summ), nrow(summ))
            summ %>%
                mutate_at(c(var1, var2), ~ .[tmp.seq]) %>% 
                group_by(!!as.name(var1), !!as.name(var2)) %>%
                select(var1, var2, matches("^(tc[1-2]{2}|alpha)")) %>%
                add_count(name = "counts") %>%
                summarize_all(mean) %>%
                rename(filter1 = var1, filter2 = var2) %>%
                ungroup() %>%
                unite(filter1, filter2, col="join", sep=".") %>%
                mutate(idxrand = ii)
        })

        ## Randomize only the level that has to be differentiated
        summ.single <- map.c(c(var1, var2), function(vrr) {
            map_dfr(seq(nrand), function(ii) {
                ## In this way var1 and var2 are randomized by keeping the pairing
                summ %>%
                    mutate_at(vrr, ~ sample(.)) %>% 
                    group_by(!!as.name(var1), !!as.name(var2)) %>%
                    select(var1, var2, matches("^(tc[1-2]{2}|alpha)")) %>%
                    add_count(name = "counts") %>%
                    summarize_all(mean) %>%
                    rename(filter1 = var1, filter2 = var2) %>%
                    ungroup() %>%
                    unite(filter1, filter2, col="join", sep=".") %>%
                    mutate(idxrand = ii)
            })
        }) 

        return(list(paired = summ.paired, single = summ.single))
    }) 

    summ.paired <- map_dfr(summ, "paired", .id = "session")
    summ.single <- map(summ, "single") %>% transpose %>% map(bind_rows, .id = "session")
    rm(summ)
    
    ## ############################################
    ## Computing the random differences, i.e. side panels

    summ.paired.diff <- list(hor = list(var = var1, lev = levels1),
                      ver = list(var = var2, lev = levels2)) %>%
        map.c(function(ls) {
            var <- ls$var
            lev <- ls$lev
            var.other <- setdiff(c(var1, var2), var)
            lev.diff <- paste0(rev(lev), collapse="_")
            diff <- summ.paired %>%
                separate(join, into = c(var1, var2), sep = "\\.") %>%
                group_by(!!as.name(var.other), idxrand) %>% ## Grouping by the vertical
                select(-counts) %>% ## Crucial removing this feature for the next step of tidyr::spread
                gather(key = "feat", value="value", matches("(tc[1-2]{2}|alpha)")) %>%
                spread(key = !!as.name(var), value = value) %>%
                mutate(!!lev.diff := !!as.name(lev[2]) - !!as.name(lev[1])) %>%
                select(-!!as.name(lev[1]), -!!as.name(lev[2])) %>%
                separate(feat, into = c("mat", "bb"), sep = "\\.") %>% ## This is slow better here
                separate(bb, into = c("HPC", "BLA"), sep = "_") %>%
                group_by(mat) %>%
                nest.c() %>%
                map(~ group_by(.x, !!as.name(var.other)) %>% nest.c) 
            return(diff)
        })


    saveRDS(summ.paired.diff, str_glue("{root.name}-tcs_scores_randomdiff.RData"))
    cat("Saved", root.name, "\n")
    rm(summ.paired)

    summ.single.diff <- list(hor = list(var = var1, lev = levels1),
                      ver = list(var = var2, lev = levels2)) %>%
        map.c(function(ls) {
            var <- ls$var
            lev <- ls$lev
            var.other <- setdiff(c(var1, var2), var)
            lev.diff <- paste0(rev(lev), collapse="_")
            diff <- summ.single[[var]] %>% ## I select the shuffling done on the `var`
                separate(join, into = c(var1, var2), sep = "\\.") %>%
                group_by(!!as.name(var.other), idxrand) %>% ## Grouping by the vertical
                select(-counts) %>% ## Crucial removing this feature for the next step of tidyr::spread
                gather(key = "feat", value="value", matches("(tc[1-2]{2}|alpha)")) %>%
                spread(key = !!as.name(var), value = value) %>%
                mutate(!!lev.diff := !!as.name(lev[2]) - !!as.name(lev[1])) %>%
                select(-!!as.name(lev[1]), -!!as.name(lev[2])) %>%
                separate(feat, into = c("mat", "bb"), sep = "\\.") %>% ## This is slow better here
                separate(bb, into = c("HPC", "BLA"), sep = "_") %>%
                group_by(mat) %>%
                nest.c() %>%
                map(~ group_by(.x, !!as.name(var.other)) %>% nest.c) 
            return(diff)
        })

    saveRDS(summ.single.diff, str_glue("{root.name}-tcs_scores_randomdiff_single.RData"))
    cat("Saved", root.name, "\n")

    invisible(list(paired = summ.paired.diff, single = summ.single.diff))
}
