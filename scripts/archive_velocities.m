%% Function to extract position and calculate the velocities; despite the title no use of whatsoever

function archive_velocities()

% rat = 'Rat08';
% session = 'Rat08-20130712';

fileID = fopen('matlab_velocity_args.txt');
args = textscan(fileID, '%s');
fclose(fileID);

rat = char(args{1}{1});
session = char(args{1}{2});

basepath = 'archive/';
dir_matlab = 'results/matlab/';

dirInfo = strcat(basepath, rat, '/', rat, '-Info/');

% All the procedure to check the consistency of the data
nsessions = load(strcat(dirInfo, rat, '-nCell'));
nsessions = size(nsessions.nCellsRight, 1);

sessionList = textscan(fopen(strcat(dirInfo, rat, '-SessionList.txt')),'%s');
sessionList = string(sessionList{1})';

if nsessions ~= length(sessionList)
    warning('MIsmatch between Number of sessions and rows of number of cells it is predictable if we are working with Rat11')
end

% Check if session is present in the session lists
if ~ismember(session, sessionList)
    error(strcat(session, ' session not found'))
end
session_num = find(contains(sessionList, session));

param_gener = LoadParameters(strcat(dirInfo, rat, '.xml'));
channels = transpose(reshape(cell2mat(param_gener.spikeGroups.groups), [8, 20])); %Matrix 20*8 with shanks assignement to each shank
tmp = xml2struct(strcat(dirInfo, rat, '.xml'));
channels_descr = tmp.parameters.generalInfo.description.Text;

% *Importing the session and check of the consistency*
cd(strcat(basepath,rat,'/',session));
xml_files = listFiles(strcat(basepath,rat,'/',session, '/*xml'));

% Check on presence of the correct xml file for settingg the session
if ~ismember(strcat(session, '.xml'), xml_files)
    error(strcat('xml file not present for this session ', join(xml_files)))
end

param_sess = LoadParameters(strcat(session, '.xml'));
% First Check on information consistency of the session
tmp = transpose(reshape((cell2mat(param_sess.spikeGroups.groups)), [8, 20]));
if ~isequal(tmp, channels)
    error('Channels on the shanks not corresponding for this session')
end

% Second Check on information consistency of the session
tmp= xml2struct(strcat(session, '.xml'));
if ~strcmp(tmp.parameters.generalInfo.description.Text, channels_descr)
    warning(strcat('data description of channels not corresponding'))
end

SetCurrentSession_optclu(strcat(session, '.xml'), false, true);
% SetCurrentSession(strcat(session, '.xml'));


% *Timestamps of the events*
% Contained here also the timestamps of Airpuff, Left Reward and Right reward
events_descr = GetEvents('output','descriptions');

max_time = max(GetEvents());


%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Extracting position and velocities

disp('Extracting positions')
pos = GetPositions('coordinates', 'real', 'pixel', 0.43, 'discard', 'partial');
if any(isnan(pos(:)))
    error('NaN values in positions')
end

% *Check if the signal has been disrupted for too much time*
gaps_vel = diff([0; pos(:,1)]);
if(any(gaps_vel > 30)) 
    warning(strcat(string(sum(gaps_vel > 30)), ' gaps between recorded position bigger than 30s. Max value is ', string(max(gaps_vel))))
end

disp('Computing velocities')

velocity_led1 = LinearVelocity(pos(:,1:3));
velocity_led2 = LinearVelocity(pos(:, [1 4:5]));

output_vel = [velocity_led1 velocity_led2(:,2)];

save(strcat(dir_matlab, session, '-position.mat'), 'pos')
save(strcat(dir_matlab, session, '-velocity.mat'), 'output_vel')
disp(strcat("Saved ", session, '-velocity.mat'))
