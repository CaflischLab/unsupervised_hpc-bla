%% Function to read the spikes and save them in friendly format

function archive_spikes()

% rat = 'Rat11';
% session = 'Rat11-20150327'

basepath = 'archive/';
dir_matlab = 'results/matlab/';

fileID = fopen('matlab_spikes_args.txt');
args=textscan(fileID, '%s');
fclose(fileID);

rat = char(args{1}{1});
session = char(args{1}{2});

dirInfo = strcat(basepath, rat, '/', rat, '-Info/');

nsessions = load(strcat(dirInfo, rat, '-nCell'));
nsessions = size(nsessions.nCellsRight, 1);

sessionList = textscan(fopen(strcat(dirInfo, rat, '-SessionList.txt')),'%s');
sessionList = string(sessionList{1})';


if nsessions ~= length(sessionList)
    warning('MIsmatch between Number of sessions and rows of number of cells it is predictable if we are working with Rat11')
end

% Check if session is present in the session lists
if ~ismember(session, sessionList)
    error(strcat(session, ' session not found'))
end
session_num = find(contains(sessionList, session));

param_gener = LoadParameters(strcat(dirInfo, rat, '.xml'));
channels = transpose(reshape(cell2mat(param_gener.spikeGroups.groups), [8, 20])); %Matrix 20*8 with shanks assignement to each shank

tmp = xml2struct(strcat(dirInfo, rat, '.xml'));
channels_descr = tmp.parameters.generalInfo.description.Text;

% *Importing the session and check of the consistency*

cd(strcat(basepath,rat,'/',session));
xml_files = listFiles(strcat(basepath,rat,'/',session, '/*xml'));

% Check on presence of the correct xml file for settingg the session
if ~ismember(strcat(session, '.xml'), xml_files)
    error(strcat('xml file not present for this session ', join(xml_files)))
end

param_sess = LoadParameters(strcat(session, '.xml'));
% First Check on information consistency of the session
tmp = transpose(reshape((cell2mat(param_sess.spikeGroups.groups)), [8, 20]));
if ~isequal(tmp, channels)
    error('Channels on the shanks not corresponding for this session')
end

% Second Check on information consistency of the session
tmp= xml2struct(strcat(session, '.xml'));
if ~strcmp(tmp.parameters.generalInfo.description.Text, channels_descr)
    warning(strcat('data description of channels not corresponding'))
end

%% Setting the current session
SetCurrentSession(strcat(session, '.xml'));
GetCurrentSession();
info = GetCurrentSession();

% *Timestamps of the events*
events_descr = GetEvents('output','descriptions');
max_time = max(GetEvents());


for anatom = ["bla_right", "bla_left", "hippo"]
    disp(strcat(rat, " ", session, " ", anatom))

    switch anatom
      case 'bla_all'
        struct_name = 'BLA';
        if strcmp(rat, 'Rat10') 
            anatom_chann = (0:127);
        elseif strcmp(rat, 'Rat08')
            anatom_chann = channels(5:20,:);
            anatom_chann = sort(anatom_chann(:));
        elseif strcmp(rat, 'Rat11')
            anatom_chann = channels(1:16,:);
            anatom_chann = sort(anatom_chann(:));
        end
      case 'bla_right'
        struct_name = 'BLA';
        if strcmp(rat, 'Rat10')
            anatom_chann = (0:63);
        elseif strcmp(rat, 'Rat08')
            anatom_chann = channels(13:20,:);
            anatom_chann = sort(anatom_chann(:));
        elseif strcmp(rat, 'Rat11')
            anatom_chann = channels(1:8,:);
            anatom_chann = sort(anatom_chann(:));
        end
      case 'bla_left'
        struct_name = 'BLA';
        if strcmp(rat, 'Rat10')
            anatom_chann = (64:127);
        elseif strcmp(rat, 'Rat08')
            anatom_chann = channels(5:12,:);
            anatom_chann = sort(anatom_chann(:));
        elseif strcmp(rat, 'Rat11')
            anatom_chann = channels(9:16,:);
            anatom_chann = sort(anatom_chann(:));
        end
      case 'hippo'
        struct_name = 'Hpc';
        if strcmp(rat, 'Rat10')
            anatom_chann = (128:159);
        elseif strcmp(rat, 'Rat08')
            anatom_chann = channels(1:4,:);
            anatom_chann = sort(anatom_chann(:));
        elseif strcmp(rat, 'Rat11')
            anatom_chann = channels(17:20,:);
            anatom_chann = sort(anatom_chann(:));
        end
    end


    %% *Assign and check the existence the channels relative to the input anatomical group*
    tmp = load(strcat(dirInfo, 'Structures/', struct_name, '.mat'));
    tmp = tmp.(struct_name);
    tmp_idx = find(tmp(:,1)==session_num); %% NO channels/group meaning these indices
    if isempty(tmp_idx) 
        error(strcat(anatom, ' not recorded in this session (it should be weird)'))        
    end
    groups_sel = transpose(tmp(tmp_idx, 2));
    channels_sel = channels(groups_sel,:);

    if ~any(ismember(channels_sel(:), anatom_chann))
        % error(strcat(anatom, ' not recorded in this session (True check)'))
        disp(strcat(anatom, ' not recorded in this session (True check)'))
        continue
    end

    channels_sel = reshape(channels_sel.', 1, []);
    channels_sel = channels_sel(ismember(channels_sel, anatom_chann));

    % Retrieving the actual group sel
    [rr,cc] = find(ismember(channels, channels_sel));
    if ~all(transpose(unique(cc)) == 1:8)
        error('Something wring with the channel indexing')
    end
    groups_sel = unique(rr);
    disp(['Number of group/shanks is ', int2str(length(groups_sel))])

    % Retrieving the spikes
    id={};
    tot={};
    for ii = 1:length(groups_sel)
        grp = groups_sel(ii);
        disp(['Group ID ', int2str(grp)])
        id{ii} = GetUnits(grp);
        tot{ii} = GetSpikeTimes([grp -1], 'output', 'full');
    end
    out = struct('id', id, 'spikes', tot);

    name_file = strcat(session, '-', anatom); 
    save(strcat(dir_matlab, name_file, '-spikes.mat'), 'out')
    disp(strcat("Saved ", name_file, "-spikes.mat"))

end
