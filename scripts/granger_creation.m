%% Calculating the granger causality values

function granger_creation()

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

dir_matlab = 'results/matlab/';
dir_granger = 'results/granger_causality/';
mvgc_dir = '~/MATLAB_packages/mvgc_v1.0'; % Directory of the MVGC package

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


by = 20;                    % evaluate GC at every <by> samples step
momax = 20;        % maximum model order for model order estimation
alpha = 0.05;      % significance level for significance test

M = 2;
wind = 10 * (momax / 4) * M^2;         % similar to SIFT documentation prescription
min_wind=min([wind 100]);  % starting to evaluate from the i=100; and
                           % progressively increase the window size up to the wind value

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Reading input from file

fileID = fopen('matlab_granger_args.txt');
args=textscan(fileID, '%s');
fclose(fileID);
session = char(args{1}{2});
anatom = char(args{1}{3});
behav = char(args{1}{4});
ncores = 6; %% How many cores in parallel

bands = {'delta'; 'theta'; 'spindles'; 'lowGamma'; 'highGamma'; 'ripples'};

disp(strcat(session, " ", anatom, " ", behav))

if ~ismember(anatom, ["bla_right", "bla_left"]) 
    error('Anatomical part not well specified.')
end

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Reading power bands values

file_hpc = strcat(session, '-hippo-', behav, '-lfp_win2s_85q.mat');
file_bla = strcat(session, '-', anatom,'-', behav, '-lfp_win2s_85q.mat');

if exist(strcat(dir_matlab, file_hpc), 'file') == 0
    error('Hpc file not existing')
end
if exist(strcat(dir_matlab, file_bla), 'file') == 0
    error('BLA file not existing')
end
    
hpc = struct2cell(load(strcat(dir_matlab, file_hpc)));
hpc = cell2struct(cellfun(@(f) getfield(hpc{1}, f), bands, 'UniformOutput', false), bands, 1);
bla = struct2cell(load(strcat(dir_matlab, file_bla)));
bla = cell2struct(cellfun(@(f) getfield(bla{1}, f), bands, 'UniformOutput', false), bands, 1);
time = struct2cell(load(strcat(dir_matlab, file_hpc)));
time = getfield(time{1}, 'time');

if unique(structfun(@(x) size(x, 2), hpc, 'UniformOutput', true)) ~= unique(structfun(@(x) size(x, 2), bla, 'UniformOutput', true)) 
    error('Input hpc and bla are different')
end

%% Setting values for analysis

resol = mean(unique(diff(time)));
wind_sec = resol*wind; 
by_sec = resol*by;

disp(strcat("Window is ", string(wind_sec), " secs long."))
disp(strcat("Step is ", string(by_sec), " secs long."))

run(strcat(mvgc_dir, 'startup.m')); 

nobs = max(size(hpc.delta));
sq = min_wind:by:nobs;

F = nan(length(bands), length(bands), 2, length(sq));
F_bad = F;
pval = F;
AIC = nan(length(bands), length(bands), length(sq));
BIC = AIC;

disp(strcat("There are ", string(length(sq)), " windows."))

%% GC calculation in parallel

parpool('local', ncores);
poolobj = gcp('nocreate');
tic;
parfor iw = 1:length(sq) 

    imax = sq(iw);
    imin = max([1 imax-wind]);

    hpcr = structfun(@(f) zscore(f(imin:imax)), hpc, 'UniformOutput', false );
    blar = structfun(@(f) zscore(f(imin:imax)), bla, 'UniformOutput', false );

    % if mod(iw,2)==0; disp(strcat("Window range ", string(imin), " ", string(imax))); end

    F_inner = nan(6,6,2);
    F_bad_inner = nan(6,6,2);
    pval_inner = nan(6,6,2);

    for bb_hpc = 1:6 
        for bb_bla = 1:6

%             disp('----------------------------------------')
%             disp('----------------------------------------')
%             disp([bb_hpc, bb_bla])
            X = vertcat(hpcr.(bands{bb_hpc}), blar.(bands{bb_bla}));
                        
            if size(X, 1) ~= 2
                error('Size of the dataset wrong')
            end

            %% Model order p evaluation
            [~, ~, moAIC, moBIC] = tsdata_to_infocrit(X, momax, 'LWR', false);

            %% Plot information criteria.
            %%         figure(1); clf;
            %%         plot_tsdata([AIC BIC]', {'AIC','BIC'});
            %%         title('Model order estimation');

            morder = moBIC;
            AIC(bb_hpc, bb_bla, iw) = moAIC;
            BIC(bb_hpc, bb_bla, iw) = moBIC;

            [Ftmp, A, SIG] = GCCA_tsdata_to_pwcgc(X, morder, 'OLS');

            if isbad(A) || isbad(Ftmp, false)
                F_inner(bb_hpc, bb_bla, :) = 1;
                F_bad_inner(bb_hpc, bb_bla, :) = 1;
                pval_inner(bb_hpc, bb_bla, :) = 1;
            else 
                F_inner(bb_hpc, bb_bla, 1) = Ftmp(2, 1);
                F_inner(bb_hpc, bb_bla, 2) = Ftmp(1, 2);
                F_bad_inner(bb_hpc, bb_bla, :) = isbad(Ftmp, false);
                
                pval_tmp = mvgc_pval(Ftmp, morder, size(X, 2), 1, 1, 1, 0, 'F'); % take careful note of arguments!
                pval_inner(bb_hpc, bb_bla, 1) = pval_tmp(2, 1);
                pval_inner(bb_hpc, bb_bla, 2) = pval_tmp(1, 2);
            end
            
        end
    end

    F(:, :, :, iw) = F_inner;
    F_bad(:, :, :, iw) = F_bad_inner;
    pval(:, :, :, iw) = pval_inner;
    if mod(iw,20)==0; disp(strcat("Done Window range ", string(imin), " ", string(imax))); end    

end
disp(strcat("Time elapsed ", string(toc), " sec."))
% poolsize = poolobj.NumWorkers,;
delete(poolobj);

index=find(F(:) < 0);
[I,J,K,L] = ind2sub(size(F),index);
index=horzcat(I,J,K,L);
disp(strcat("Number of NaNs found is ", string(size(index, 1))));

%% Saving files
name_file = strcat(session, '-win2s_85q-', anatom, '-', behav, '-w', string(wind), '-step', string(by),'-granger.mat'); 

output = struct('F', F, 'Fbad', F_bad, 'pval', pval, 'AIC', AIC, 'BIC', BIC, 'time', time(sq), 'wind_sec', wind_sec, 'step_sec', by_sec);
save(strcat(dir_granger, name_file), 'output');
disp(strcat('Saved ', dir_granger, name_file))




